﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class PushableButtonScript : MonoBehaviour {
	public UnityEvent trigger;
	bool triggered = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D c) {
		print("Collision, name of collided object is " + c.gameObject.name);
		if (c.gameObject.tag == "PushableButton" && triggered == false) {
			print("triggered");
			triggered = true;
			trigger.Invoke();
		}
	}
}
