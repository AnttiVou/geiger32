﻿using UnityEngine;
using System.Collections;

public class RagDollBock : MonoBehaviour {
	public GameObject ragDollBlock;
	bool collHappened;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D c) {
		if (c.gameObject.name == ragDollBlock.name && !collHappened) {
			collHappened = true;
			var obj = c.gameObject.transform.Find("Checker");
			var script = obj.GetComponent<AndyPush>();
			script.pushEnded = true;
			Rigidbody2D rg = c.gameObject.GetComponent<Rigidbody2D>();
			rg.isKinematic = false;
		}
	}
}
