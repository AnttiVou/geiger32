﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoBehaviour {
	float fadeTime = 1f;
	float originalFadeTime = 1f;
	bool fading;
	bool switching;
	public AudioSource audioSource;
	public List<AudioClip> musicClips;
	int musicClipInt;

	// Use this for initialization
	void Start () {
		originalFadeTime = fadeTime;
	
	}
	
	// Update is called once per frame
	void Update () {
		if (switching) {
			fading = true;
			switching = false;
			fadeTime = originalFadeTime;
		}
		if (fading) {
			fadeTime -= Time.deltaTime;
			audioSource.volume -= Time.deltaTime;
			if (fadeTime <= 0) {
				audioSource.Stop();
				if (musicClips[musicClipInt] != null) {
					audioSource.clip = musicClips[musicClipInt];
					audioSource.volume = 1f;
					audioSource.Play();
				}
				fading = false;
				switching = false;
			}
		}
	
	}

	public void SwitchMusic(int i) {
		musicClipInt = i;
		switching = true;
	}
}
