﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ToggleScript : MonoBehaviour {
	PlayerControl playerControl;
	GameObject gameManager;
	public GameObject player;
	public GameObject playerThatCanActivateThis;
	public List<GameObject> gameObjectsToToggle;
	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager");
		playerControl = gameManager.GetComponent<PlayerControl>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Toggle() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplToggle");
	}

	public void ImplToggle() {
		if (gameObjectsToToggle.Count > 0) {
			for (int i = 0; i < gameObjectsToToggle.Count; i++) {
				if (gameObjectsToToggle[i] != null) {
					if (gameObjectsToToggle[i].activeSelf == true) {
						gameObjectsToToggle[i].SetActive(false);
					}
					else if (gameObjectsToToggle[i].activeSelf == false) {
						gameObjectsToToggle[i].SetActive(true);
					}
				}
			}
		}

	}
}
