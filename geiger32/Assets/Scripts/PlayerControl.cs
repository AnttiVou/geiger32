﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerControl : MonoBehaviour
{
	//[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	//[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.


	public float moveForce = 365f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 5f;				// The fastest the player can travel in the x axis.
	public AudioClip[] jumpClips;			// Array of clips for when the player jumps.
	public float jumpForce = 1000f;			// Amount of force added when the player jumps.

	
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	public bool grounded = false;           // Whether or not the player is grounded.
	public bool climbing;
	public Animator anim;                   // Reference to the player's animator component.
	public GameObject player;
	public GameObject cameraObject;
	public List<GameObject> selectionOfCharacters;
	public List<GameObject> characters;
	public GameObject talkUi;
	int currentCharacterInt = 0;
	public bool switchAllowed = true;
	bool switchAllowedHidden;
	public Collider2D stephCollider;
         


	void Awake()
	{
		// Setting up references.
		groundCheck = player.transform.Find("groundCheck");
		anim = player.GetComponent<Animator>();
	}


	void Update()
	{
       
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		grounded = Physics2D.Linecast(player.transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));  

		// If the jump button is pressed and the player is grounded then the player should jump.
		if(Input.GetButtonDown("Jump") && grounded)
			jump = true;

		if (Input.GetKeyDown(KeyCode.M) && !climbing && grounded && switchAllowed) {
			PrepSwitch();
		}
	}


	void FixedUpdate() {
		// Cache the horizontal input.
		float h = Input.GetAxis("Horizontal");
			if ((h > 0f || h < 0f) && h != 0f) {
			if (!climbing) {
				anim.SetInteger("Walk", 1);
			}
			} else {
			if (!climbing) {
				anim.SetInteger("Walk", 0);
			}

				//if (grounded) {
				//player.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
				//}
			}
		

		// The Speed animator parameter is set to the absolute value of the horizontal input.
		//anim.SetFloat("Speed", Mathf.Abs(h));

		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * player.GetComponent<Rigidbody2D>().velocity.x < maxSpeed)
			// ... add a force to the player.
			player.GetComponent<Rigidbody2D>().AddForce(Vector2.right * h * moveForce);

		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(player.GetComponent<Rigidbody2D>().velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			player.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(player.GetComponent<Rigidbody2D>().velocity.x) * maxSpeed, player.GetComponent<Rigidbody2D>().velocity.y);

		// If the input is moving the player right and the player is facing left...
		if(h > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h < 0 && facingRight)
			// ... flip the player.
			Flip();

		// If the player should jump...
		if(jump)
		{
			// Set the Jump animator trigger parameter.
			//anim.SetTrigger("Jump");

			// Play a random jump audio clip.
			//int i = Random.Range(0, jumpClips.Length);
			//AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);

			// Add a vertical force to the player.
			player.GetComponent<Rigidbody2D>().AddForce(new Vector2(0f, jumpForce));

			// Make sure the player can't jump again until the jump conditions from Update are satisfied.
			jump = false;
		}
	}
	
	
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = player.transform.localScale;
		theScale.x *= -1;
		player.transform.localScale = theScale;
	}

	public void SwitchPlayer () {
		
		groundCheck = player.transform.Find("groundCheck");
		anim = player.GetComponent<Animator>();
		Rigidbody2D rg = player.GetComponent<Rigidbody2D>();
		rg.isKinematic = false;
		Collider2D c = player.GetComponent<Collider2D>();
		if (player.name != "Steph") {
			c.enabled = true;
		}

		if (player.name == "Steph") {
			stephCollider.enabled = true;
		}
	}

	public void PrepSwitch() {
		
		MarkerManagerScript markerScript = player.GetComponent<MarkerManagerScript>();
		markerScript.ResetMarker();
		currentCharacterInt++;
		Vector3 theScale = player.transform.localScale;
		theScale.x = 1;
		player.transform.localScale = theScale;
		facingRight = true;
		if (currentCharacterInt >= characters.Count) {
			currentCharacterInt = 0;
		}
		anim.SetInteger("Walk", 0);
		Rigidbody2D rg = player.GetComponent<Rigidbody2D>();
		rg.isKinematic = true;
		Collider2D c = player.GetComponent<Collider2D>();
		if (player.name == "Steph") {
			stephCollider.enabled = false;
		}
		if (player.name != "Steph") {
			c.enabled = false;
		}
		player = characters[currentCharacterInt];
		var camScript = cameraObject.GetComponent<CameraScript>();
		camScript.currentPlayer = player.transform;
		/*
		if (talkUi != null) {
			talkUi.SetActive(false);
		}
		*/
		SwitchPlayer();
	}

	public void AllowSwitch(bool b) {
		switchAllowedHidden = b;
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplAllowSwitch");
	}
	public void ImplAllowSwitch() {
		switchAllowed = switchAllowedHidden;
	}

	public void PlayerSwitch() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplPlayerSwitch");
	}
	public void ImplPlayerSwitch() {
		PrepSwitch();
	}



}
