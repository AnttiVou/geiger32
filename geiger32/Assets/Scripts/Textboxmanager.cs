﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Textboxmanager : MonoBehaviour {
    public GameObject Textbox;

    public Text theText;

    public TextAsset textfile;
    public string[] textlines;

    public int currentLine;
    public int endAtLine;

    public PlayerControl player;

    public bool isActive;

    public bool stopPlayerMovement;

    private bool istypeing = false;
    private bool canceltyping = false;

    public float typeingSpeed;


    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerControl>();

        if (textfile != null)
        {
            textlines = (textfile.text.Split('\n'));
        }

        if (endAtLine == 0)
        {
            endAtLine = textlines.Length - 1;

        }

        if (isActive)
        {
            EnabledTexBox();
        }
        else
        {
            DisableTextBox();
        }
    }

    void Update()
    {
        if(!isActive)
        {
            return;
        }

            //theText.text = textlines[currentLine];
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (!istypeing)
            {
                currentLine += 1;

            
            if (currentLine > endAtLine)
            {
                DisableTextBox();
            }
            else
            {
                StartCoroutine(TextScroll(textlines[currentLine]));

            }
        }
        else if (istypeing && !canceltyping)
        {
            canceltyping = true;
        }
      }
    }

        private IEnumerator TextScroll (string lineoftext)
        {
        int letter = 0;
        theText.text = ""; //Black screen appears
        istypeing = true;
        canceltyping = false;
        while (istypeing && !canceltyping && (letter < lineoftext.Length - 1))
        {
            theText.text += lineoftext[letter];
            letter += 1;
            yield return new WaitForSeconds(typeingSpeed);
        }
        theText.text = lineoftext;
        istypeing = false;
        canceltyping = true;
    }


    public void EnabledTexBox()
    {
        Textbox.SetActive(true);
        isActive = true;

        if(stopPlayerMovement)
        {
          
        }
    }
    public void DisableTextBox()
    { 
        Textbox.SetActive(false);
        isActive = false;

        
        }
    public void ReloadScript(TextAsset theText)
    {
        if(theText != null)
        {
            textlines = new string[1];
            textlines = (theText.text.Split('\n'));
        }
    }
}
