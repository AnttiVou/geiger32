﻿using UnityEngine;
using System.Collections;

public class ActivateText : MonoBehaviour {

    public TextAsset theText;

    public int startLine;
    public int endline;

    public Textboxmanager theTextBox;
    public bool destroyWhenActivated;


	// Use this for initialization
	void Start () {
        theTextBox = FindObjectOfType<Textboxmanager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Jamppa")
        {
            theTextBox.ReloadScript(theText);
            theTextBox.currentLine = startLine;
            theTextBox.endAtLine = endline;
            theTextBox.EnabledTexBox();

            if(destroyWhenActivated)
            {
                Destroy(gameObject);
            }
        }
    }
}
