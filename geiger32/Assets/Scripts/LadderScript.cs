﻿using UnityEngine;
using System.Collections;

public class LadderScript : MonoBehaviour {
	public GameObject ladderPlatform;
	public GameObject ladderHighPoint;
	public GameObject ladderLowPoint;
	public GameObject climber;
	public GameObject gameManager;
	public float climbSpeed = 3f;
	public bool isClimbing;
	PlayerControl playerScript;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager");
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float v = Input.GetAxis("Vertical");
		if (climber != null) {
			playerScript.climbing = true;
			Animator anim = climber.GetComponent<Animator>();
			if ((v > 0f || v < 0f) && v != 0f) {
				anim.speed = 1f;
				anim.SetInteger("Walk", 2);
				if (v > 0f) {
					ladderPlatform.transform.position = Vector2.MoveTowards(ladderPlatform.transform.position, ladderHighPoint.transform.position, climbSpeed * Time.deltaTime);
				} else if (v < 0f) {
					ladderPlatform.transform.position = Vector2.MoveTowards(ladderPlatform.transform.position, ladderLowPoint.transform.position, climbSpeed * Time.deltaTime);
				}
			}
			else {
				anim.speed = 0f;

			}
		}
	}

	void OnTriggerEnter2D(Collider2D c) {
		if (c.gameObject.tag == "Player" && c.gameObject.name != "David") {
			ladderPlatform.SetActive(true);
			climber = c.gameObject;
			playerScript = gameManager.GetComponent<PlayerControl>();
			GameObject ladderPlatPoint = climber.transform.FindChild("LadderPlacementPos").gameObject;
			ladderPlatform.transform.position = new Vector3(gameObject.transform.position.x, ladderPlatPoint.transform.position.y, ladderPlatPoint.transform.position.z);
			climber.transform.parent = ladderPlatform.transform;
			isClimbing = true;
			playerScript.climbing = true;
			Animator anim = climber.GetComponent<Animator>();
			anim.SetInteger("Walk", 2);
			anim.speed = 1f;
		}
	}
	void OnTriggerExit2D(Collider2D c) {
		if (c.gameObject.tag == "Player" && c.gameObject.name != "David") {
			Animator anim = climber.GetComponent<Animator>();
			ladderPlatform.SetActive(false);
			anim.speed = 1f;
			climber.transform.parent = null;
			GameObject ladderPlatPoint = climber.transform.FindChild("LadderPlacementPos").gameObject;
			ladderPlatform.transform.position = ladderLowPoint.transform.position;
			playerScript.climbing = false;
			playerScript = null;
			climber = null;
			isClimbing = false;
		}
	}
}
