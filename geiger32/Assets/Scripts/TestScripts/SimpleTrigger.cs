﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using System.Collections.Generic;

[System.Serializable]
public class MunEvent : UnityEvent<string, string> { }

[System.Serializable]
public struct doubleString {
	public string a;
	public string b;
}


public class SimpleTrigger : MonoBehaviour {
	/*public List<doubleString> tuplat;

	public UnityEvent testEvent;
	public UnityEvent abctest;
	public MunEvent munEvent;
	public doubleString d;
	*/
	public bool turnOffAfterEnter;
	bool turnedOffEnter = false;
	bool turnedOffUse = false;
	public bool turnOffAfterUse;
	public List<bool> hasUsed;
	public bool multipleUses;
	//lista kaikista pelaajahahmoista
	//lista pelaajahahmoista joilla
	public List<UnityEvent> onEnter;
	public List<UnityEvent> onUse;
	public List<UnityEvent> onLeave;
	public PlayerIndex index;
	public GameObject player;
	Rigidbody2D rgb;
	ActivateScript activateScript;
	public bool someoneIsIn;
	PlayerControl playerControl;

	void Start() {
		//hasUsed.Capacity = 4;
		for (int i = 0; i < 5; i++) {
			hasUsed.Add(false);
		}

		if (this.name == "ColliderTest")print("" + onEnter[0].GetPersistentEventCount() + onEnter[1].GetPersistentEventCount());
		playerControl = GameObject.Find("GameManager").GetComponent<PlayerControl>();

	}

	void Update () {
		if (playerControl.player != player) {
			someoneIsIn = false;
		}
		if (someoneIsIn) {
			if (Input.GetKeyDown(KeyCode.X)) {
				ExecuteFunctions();
			}
			}
		
	}
	void ExecuteFunctions() {
		print("executed");	
		if (index != null) {
			print("ei oo nll1");
			if (!hasUsed[index.index]) {
				print("ei oo nll1.5");
				if (onUse[index.index].GetPersistentEventCount() != 0) {
					print("ei oo nll2");
					onUse[index.index].Invoke();
				}
				/*
				if (onUse[4] != null && onUse[index.index].GetPersistentEventCount() == 0) {
					print("ei oo nll2.5");
					if (onUse[4].GetPersistentEventCount() >= 1) {
						print("ei oo nll3");
						print(onUse[4].GetPersistentEventCount());
						onUse[4].Invoke();
					}

				}
				*/
				//onUse[index.index].Invoke();
				if (turnOffAfterUse) {
					gameObject.SetActive(false);
				}
				/*
				if (!multipleUses) {
					hasUsed[index.index] = true;
				}
				*/
			}
			
			if (!multipleUses) {
				hasUsed[index.index] = true;
			}
			

		}
	}

	//TODO: Reagoi vain eri pelaajiin
	void OnTriggerEnter2D(Collider2D c) {
		//testEvent.Invoke();
		//munEvent.Invoke("andy", "woah");
		//looppaa kaikki pelaajat
		//tsiigaa et 
		if (!turnedOffEnter && c.tag == "Player") {
			player = c.gameObject;
			index = player.GetComponent<PlayerIndex>();
			/*
			activateScript = c.gameObject.GetComponent<ActivateScript>();
			activateScript.player = c.gameObject;
			*/
			if (index != null) {
				print("ei oo nll1");
				if (onEnter[index.index] != null) { 
					 if	(onEnter[index.index].GetPersistentEventCount() != 0) {
						print("ei oo nll2");
						onEnter[index.index].Invoke();
					}
				}
				/*
				if (onEnter[4] != null && onEnter[index.index].GetPersistentEventCount() == 0) {
					print("ei oo nll3");
					if (onEnter[4].GetPersistentEventCount() >= 1) {
						onEnter[4].Invoke();
					}
				}
				*/
			}
			if (turnOffAfterEnter) {
				turnedOffEnter = true;
			}
		}


	}
	void OnTriggerStay2D(Collider2D c) {
		if (c.tag == "Player") {
			//index = c.gameObject.GetComponent<PlayerIndex>();
			player = c.gameObject;
			index = player.GetComponent<PlayerIndex>();
			rgb = player.GetComponent<Rigidbody2D>();
			if (playerControl.player == player) {
				someoneIsIn = true;
			}
			else if (playerControl.player != player) {
				someoneIsIn = false;
			}
		}
	}
	void OnTriggerExit2D(Collider2D c) {
		if (c.tag == "Player") {
			someoneIsIn = false;
			if (index != null) {
				print("ei oo nll1");
				
				if (onLeave[index.index] != null) {
					if (onLeave[index.index].GetPersistentEventCount() != 0) {
						print("ei oo nll2");
						onLeave[index.index].Invoke();
					}
				}
				/*
				if (onLeave[4] != null && onLeave[index.index].GetPersistentEventCount() == 0) {
					print("ei oo nll3");
					if (onLeave[4].GetPersistentEventCount() >= 1) {
						onLeave[4].Invoke();
					}
				}
				*/
			}
		}
	}

}
