﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FeedPlayerInfo : MonoBehaviour {

	public List<string> textPrints;
	public GameObject talkUIObject;
	public GameObject talkUi;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Relay(int b) {
		if (talkUi != null) {
			talkUi.SetActive(true);
		}
		var textScript = GetComponent<TextPrints>();
		var playerScript = GetComponent<PlayerIndex>();
		int i = playerScript.index;
		print(textPrints[b]);
		var uiFunc = talkUIObject.GetComponent<TalkUiScript>();
		uiFunc.SendUiInfo(i, textPrints[b]);
		//textScript.textPrints[b];


	}
}
