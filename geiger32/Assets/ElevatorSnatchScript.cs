﻿using UnityEngine;
using System.Collections;

public class ElevatorSnatchScript : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D (Collider2D c) {
		if (c.gameObject.tag == "Player") {
			c.transform.parent = gameObject.transform;
		}
	}
	void OnTriggerExit2D(Collider2D c) {
		if (c.gameObject.tag == "Player") {
			c.transform.parent = null;
		}
	}
}
