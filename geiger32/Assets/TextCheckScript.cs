﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class TextCheckScript : MonoBehaviour {

	public string correctAnswer;
	public string currentAnswerInput;
	public Text text;
	public int stringLength;
	int originalStringLength;
	bool puzzleComplete;
	public List<GameObject> turnOffObjects;
	public List<GameObject> turnOnObjects;
	

	// Use this for initialization
	void Start () {
		originalStringLength = correctAnswer.Length;
	}
	
	// Update is called once per frame
	void Update () {
		stringLength = currentAnswerInput.Length;
		if (stringLength > originalStringLength) {
			currentAnswerInput = "";
		}
		text.text = currentAnswerInput;
		if (!puzzleComplete && text.text == correctAnswer) {
			puzzleComplete = true;
			PuzzleComplete();
		}

	}

	public void AddToAnswer(string s) {
		currentAnswerInput = currentAnswerInput + s;

	}

	public void PuzzleComplete() {
		print("we won");
		for (int i = 0; i < turnOnObjects.Count; i++) {
			if (turnOnObjects[i] != null) {
				turnOnObjects[i].SetActive(true);
			}
		}
		for (int i = 0; i < turnOffObjects.Count; i++) {
			if (turnOffObjects[i] != null) {
				turnOffObjects[i].SetActive(false);
			}
		}
	}
}
