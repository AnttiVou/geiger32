﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FootStepEventScript : MonoBehaviour {
	AudioSource audioSource;
	public List<AudioClip> footStep;
	// Use this for initialization
	void Awake() {
		audioSource = GetComponent<AudioSource>();
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlaySound() {
		int i = Random.Range(0, footStep.Count);
		audioSource.clip = footStep[i];
		audioSource.Play();
	}
}
