﻿using UnityEngine;
using System.Collections;

public class AnimateScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Animate() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplAnimate");



	}
	public void ImplAnimate() {
		Animator anim = GetComponent<Animator>();
		anim.SetInteger("Operate", 1);
	}
}
