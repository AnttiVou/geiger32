﻿using UnityEngine;
using System.Collections;

public class IntroScript : MonoBehaviour {
	public GameObject canvas;
	Animator anim;
	public int mapInt;

	bool started;
	// Use this for initialization
	void Start () {
		anim = canvas.GetComponent<Animator>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void StartIntro() {
		if (!started) {
			started = true;
			anim.SetInteger("Operate", 1);
			


		}

	}
	public void LoadMap() {
		Application.LoadLevel(mapInt);
	}
}
