﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActivateScript : MonoBehaviour {
	PlayerControl playerControl;
	public GameObject player;
	public GameObject playerThatCanActivateThis;
	GameObject GameManager;
	public List<GameObject> GameobjectToActivate;
	public List<GameObject> GameobjectToDeactivate;
	public bool turnOffSelf = true;

	// Use this for initialization
	void Start () {
		GameManager = GameObject.Find("GameManager");
		playerControl = GameManager.GetComponent<PlayerControl>();
	
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if (playerThatCanActivateThis == null) {
			playerThatCanActivateThis = player;
		}
		if (Input.GetButtonDown("Fire1") && player == playerControl.player && player == playerThatCanActivateThis) {
			print("test");
			DeActivateGameObjects();
			ActivateGameObjects();
		}
		*/
	
	}

	public void RunFunctions() {
		DeActivateGameObjects();
		ActivateGameObjects();
	}

	public void DeActivateGameObjects() {
		if (GameobjectToDeactivate.Count > 0) {
			for (int i = 0; i < GameobjectToDeactivate.Count; i++) {
				if (GameobjectToDeactivate[i] != null) {
					GameobjectToDeactivate[i].SetActive(false);
				}
			}
		}

	}

	public void ActivateGameObjects() {
		if (GameobjectToActivate.Count > 0) {
			for (int i = 0; i < GameobjectToActivate.Count; i++) {
				if (GameobjectToActivate[i] != null) {
					/*
					MarkerManagerScript markerScript = player.GetComponent<MarkerManagerScript>();
					markerScript.ResetMarker();
					*/
					GameobjectToActivate[i].SetActive(true);
					if (turnOffSelf) {
						gameObject.SetActive(false);
					}
				}
			}
		}
	}
}
