﻿using UnityEngine;
using System.Collections;

public class RestartManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.F5)) {
			Restart();
		}
	
	}

	public void Restart() {
		Application.LoadLevel(0);
	}
}
