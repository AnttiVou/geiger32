﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriggerScript : MonoBehaviour {

	public List<string> markerName;
	ActivateScript activateScript;
	public List<GameObject> characters;



	// Use this for initialization
	void Start () {
		activateScript = GetComponent<ActivateScript>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Relay (GameObject p) {
		activateScript.player = p;
	}
	
}
