﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class MarkerManagerScript : MonoBehaviour {
	public string markerName;
	public GameObject marker;
	PlayerControl playerControl;
	GameObject GameManager;
	int a;



	void Start () {
		GameManager = GameObject.Find("GameManager");
		playerControl = GameManager.GetComponent<PlayerControl>();

	}
	//TODO: Reagoi vain eri pelaajiin
	void OnTriggerStay2D(Collider2D c) {
		if (c.tag == "Trigger" && playerControl.player == gameObject) {
			TriggerScript script = c.gameObject.GetComponent<TriggerScript>();
			for (int i = 0; i < script.characters.Count; i++) {
				if (script.characters[i] == gameObject) {
					a = i;
				}
			}
			markerName = script.markerName[a];
			if (markerName != null) {
				marker = transform.FindChild(markerName).gameObject;
				marker.SetActive(true);
			}
			script.Relay(gameObject);

		}
	}
	void OnTriggerExit2D(Collider2D c) {
		print("leaving");
		if (c.tag == "Trigger") {
			ResetMarker();
			TriggerScript script = c.gameObject.GetComponent<TriggerScript>();
			script.Relay(null);

		}
	}

	public void ResetMarker () {
		if (marker != null && marker != gameObject) {
			marker.SetActive(false);
			marker = null;
		}
		markerName = "";
	}

}
