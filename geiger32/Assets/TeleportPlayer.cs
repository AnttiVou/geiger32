﻿using UnityEngine;
using System.Collections;

public class TeleportPlayer : MonoBehaviour {

	Transform playerToBring;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BringPlayer(Transform t) {
		playerToBring = t;
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplBringPlayer");
	}

	public void ImplBringPlayer() {
		playerToBring.position = transform.position;
	}
}
