﻿using UnityEngine;
using System.Collections;

public class AddPlayerScript : MonoBehaviour {
	public int playerNumber;
	GameObject gameManager;
	PlayerControl playerControl;
	GameObject characterToAdd;
	//GameObject character;

	// Use this for initialization

	void Awake () {
		gameManager = GameObject.Find("GameManager");
		playerControl = gameManager.GetComponent<PlayerControl>();
		//character = characterToAdd;
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void AddCharacter(GameObject g) {
		//testEvent.Invoke();
		//munEvent.Invoke("andy", "woah");
		//looppaa kaikki pelaajat
		//tsiigaa et 
		characterToAdd = g;
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplAddCharacters");


	}

	public void ImplAddCharacters() {
		playerControl.characters.Add(characterToAdd);
		print("Character added: " + characterToAdd);
		//print("In reality we added: " + character);
		gameObject.SetActive(false);
	}
}
