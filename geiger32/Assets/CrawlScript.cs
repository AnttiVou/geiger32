﻿using UnityEngine;
using System.Collections;

public class CrawlScript : MonoBehaviour {

	Animator anim;
	GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (player != null) {
			float v = Input.GetAxis("Horizontal");
			if ((v > 0f || v < 0f) && v != 0f) {
				anim.speed = 1f;
			}
			else {
				anim.speed = 0f;

			}
		}

	}

	public void Crawl() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplCrawl");
	}

	public void ImplCrawl() {
		PlayerControl pcont = GameObject.Find("GameManager").GetComponent<PlayerControl>();
		pcont.switchAllowed = false;
		player = pcont.player;
		anim = player.GetComponent<Animator>();
		anim.SetInteger("Crawl", 1);
        var collider1 = player.GetComponent<PolygonCollider2D>();
        var collider2 = player.GetComponent<BoxCollider2D>();
        if (collider1 != null)
        {
            collider1.enabled = false;
        }
        if (collider2 != null)
        {
            collider2.enabled = true;
        }
    }

	public void StopCrawl() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplStopCrawl");
	}

	public void ImplStopCrawl() {
		PlayerControl pcont = GameObject.Find("GameManager").GetComponent<PlayerControl>();
		player = pcont.player;
		anim = player.GetComponent<Animator>();
		anim.SetInteger("Crawl", 0);
        var collider1 = player.GetComponent<PolygonCollider2D>();
        var collider2 = player.GetComponent<BoxCollider2D>();
        if (collider1 != null)
        {
            collider1.enabled = true;
        }
        if (collider2 != null)
        {
            collider2.enabled = false;
        }
        player = null;
        anim.speed = 1f;
		pcont.switchAllowed = true;
	}
}
