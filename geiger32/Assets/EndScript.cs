﻿using UnityEngine;
using System.Collections;

public class EndScript : MonoBehaviour {
	public int sceneToEnter;
	public float timeToEnd = 20f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		timeToEnd -= Time.deltaTime;
		if (timeToEnd <= 0) {
			Application.LoadLevel(sceneToEnter);
		}
	
	}
}
