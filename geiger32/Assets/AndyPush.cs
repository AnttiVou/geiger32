﻿using UnityEngine;
using System.Collections;

public class AndyPush : MonoBehaviour {
	PlayerIndex playerIndex;
	public Rigidbody2D rg;
	bool pushing;
	Animator anim;
	public bool pushEnded;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (pushing && anim != null) {
			float v = Input.GetAxis("Horizontal");
			if ((v > 0f || v < 0f) && v != 0f) {
				anim.speed = 1f;
			}
			else anim.speed = 0f;
		}
	
	}

	void OnTriggerEnter2D(Collider2D c) {
		if (c.tag == "Player") {
			playerIndex = c.gameObject.GetComponent<PlayerIndex>();
			if (playerIndex.index == 0) {
				pushing = true;
				anim = c.gameObject.GetComponent<Animator>();
				anim.SetInteger("Push", 1);
				rg.isKinematic = false;
			}
		}
	}
	void OnTriggerExit2D(Collider2D c) {
		if (c.tag == "Player") {
			playerIndex = c.gameObject.GetComponent<PlayerIndex>();
			if (playerIndex.index == 0) {
				pushing = false;
				anim = c.gameObject.GetComponent<Animator>();
				anim.SetInteger("Push", 0);
				if (pushEnded) {
					rg.isKinematic = true;
				}
				anim.speed = 1f;
				anim = null;
			}
		}
	}
}
