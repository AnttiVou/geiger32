﻿using UnityEngine;
using System.Collections;

public class CraneGrabScript : MonoBehaviour {
	PlayerControl playerControl;
	GameObject grabbedPlayer;
	bool operated;

	// Use this for initialization
	void Start () {
		playerControl = GameObject.Find("GameManager").GetComponent<PlayerControl>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D c) {
		//if (!operated) {
			print("Collision with " + c.gameObject.name);
			if (c.gameObject.tag == "PlayerGrabPos" && c.gameObject.name == "MagnetGrabPos" && grabbedPlayer == null) {
				grabbedPlayer = c.transform.parent.gameObject;
				grabbedPlayer.transform.parent = gameObject.transform;
			}
			if (c.gameObject.name == "ReleasePos" && grabbedPlayer != null) {
				grabbedPlayer.transform.parent = null;
				playerControl.PlayerSwitch();
				//operated = true;
			}
		//}
	}
}
