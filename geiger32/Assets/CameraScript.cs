﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraScript : MonoBehaviour {


	public Transform currentPlayer;
	Transform lastPlayer;
	Transform focusPos;
	public Transform room;
	Collider2D roomCollider;
	public Collider2D newRoom;
    public Transform cameraObject;
    public float intensity = 0.3f;
    bool shakeCamera;
	// Use this for initialization
	void Start () {
		roomCollider = room.GetComponent<Collider2D>();
        //cameraObject.transform.position = new Vector2(transform.position.x + Random.Range(-intensity, intensity), transform.position.y + Random.Range(-intensity, intensity));

    }
	
	// Update is called once per frame
    

	void Update () {
		float camExtentV = Camera.main.orthographicSize;
		float camExtentH = (camExtentV * Screen.width) / Screen.height;

		Bounds bounds = roomCollider.bounds;

		float leftBound = bounds.min.x + camExtentH;
		float rightBound = bounds.max.x - camExtentH;
		float bottomBound = bounds.min.y + camExtentV;
		float topBound = bounds.max.y - camExtentV;

		if (currentPlayer) {
			var v3 = currentPlayer.transform.position;
			v3.x = Mathf.Clamp(v3.x, leftBound, rightBound);
			v3.y = Mathf.Clamp(v3.y, bottomBound, topBound);
			transform.position = v3;
		}
		//Collider2D newRoom;
		newRoom = Physics2D.OverlapPoint(currentPlayer.transform.position, 1 << LayerMask.NameToLayer("CameraRooms"));

		if (newRoom != null && newRoom != roomCollider) {
			print("found new room (" + newRoom.gameObject.name + ")");
			roomCollider = newRoom;
		}
        //Camera Shake

        if (shakeCamera)
        {
            cameraObject.transform.position = new Vector3(transform.position.x + Random.Range(-intensity, intensity), transform.position.y + Random.Range(-intensity, intensity), -69);
        }


		//physics2d overlappoint
		//if (currentPlayer.transform.collider2D == Physics2D.Ove)

	}

	public void CamSwitch(Transform g) {
		focusPos = g;
		lastPlayer = currentPlayer;
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplCamSwitch");

	}

	public void ImplCamSwitch() {
		currentPlayer = focusPos;
	}

	public void ReturnSwitch() {
		var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
		scheduler.InvokeLater(this, "ImplReturnSwitch");
	}
	public void ImplReturnSwitch() {
		currentPlayer = lastPlayer;
	}

    public void StartShake(float f)
    {
        intensity = f;
        var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
        scheduler.InvokeLater(this, "ImplStartShake");
    }
    public void ImplStartShake()
    {
        shakeCamera = true;
    }
    public void EndShake()
    {
        var scheduler = GameObject.Find("Scheduler").GetComponent<Scheduler>();
        scheduler.InvokeLater(this, "ImplEndShake");
    }
    public void ImplEndShake()
    {
        shakeCamera = false;
        cameraObject.transform.position = new Vector3(transform.position.x, transform.position.y, -69);

    }
}
